<?php
/**
 * @package Modules
 * @subpackage AdministratorScreens
 */
class SliderController extends ActionsController {
	
	function edit_method(){
		
		$Register = &Register::getInstance();
		/*@var $Register Register*/
		$smarty = &$Register->get(VAR_SMARTY);
		/*@var $smarty Smarty*/
		
		$PID = $this->getData('pid');
		$SID = $this->getData('sid');

		$q = db_phquery("SELECT * FROM 1_slider WHERE id = ?",$SID);
		$row=db_fetch_row($q);
		
		$smarty->assign('data', $row);
		$smarty->assign('PID', $PID);
		$smarty->assign('SID', $SID);
		$smarty->assign('admin_sub_dpt', 'slider_edit.html');
	}
	
	
    function save_order(){
    
        $scan_result = scanArrayKeysForID($_POST, 'priority');
        $sql = '
            UPDATE 1_slider SET sort_order=? WHERE id=?
        ';
        
        foreach ($scan_result as $slides_id=>$scan_info){
            
            db_phquery($sql, $scan_info['priority'] + 1, $slides_id);
        }
        
        Message::raiseAjaxMessage(MSG_SUCCESS, '', 'order_saved');
        die;
    }
	
	function add_method(){
	
		renderURL('action=add_method', '', true);
		
		$Register = &Register::getInstance();
		/*@var $Register Register*/
		$smarty = &$Register->get(VAR_SMARTY);

		$smarty->assign('data', $default_data);

			
		$smarty->assign('admin_sub_dpt', 'slider_add.html');
	}
	
	function delete_method(){

		$SID = $this->getData('sid');
		if (db_phquery('DELETE FROM 1_slider WHERE id=?', $SID))
		Message::raiseMessageRedirectSQ(MSG_SUCCESS, '', "Слайд удалён");
	}
	
	function save_method(){

		safeMode(true);

		$refresh = $this->existsData('save');
		$SID = $this->getData('sid');

		$shipping_method = shGetShippingMethodById($SID);
		if(!$shipping_method)RedirectSQ();

		$shippingModule = ShippingRateCalculator::getInstance($shipping_method['module_id']);

		$_POST['save'] = $refresh?'refresh':1;



		if($refresh){
			Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'action=edit_method', '', '', array('name' => 'refresh', 'Data' => $this->getData()));
		}else{

/*			if(empty($_POST['url'])){
				Message::raiseMessageRedirectSQ(MSG_ERROR, 'action=edit_method', 'Введите URL страницы', '', array('Fields' => 'url','Data' => $this->getData()));
			}*/
			if(empty($_POST['url_image'])){
				Message::raiseMessageRedirectSQ(MSG_ERROR, 'action=edit_method', 'Введите URL файла картинки', '', array('Fields' => 'url','Data' => $this->getData()));
			}
			
                if ($_POST['save'] == 1) {
                    if ($_POST['type'] == 'edit') {
                        $sql = 'UPDATE 1_slider SET `name`=?, `description`=?, `url`=?, `url_image`=?	WHERE id=?';
                        db_phquery_array($sql, $_POST['name'], $_POST['description'],$_POST['url'], $_POST['url_image'], $SID);
                    }
                    if ($_POST['type'] == 'add') {
                        $sql = 'INSERT INTO 1_slider SET `name`=?, `description`=?, `url`=?, `url_image`=?';
                        db_phquery_array($sql, $_POST['name'], $_POST['description'],$_POST['url'], $_POST['url_image']);
                    }
                }			
			
			Message::raiseMessageRedirectSQ(MSG_SUCCESS, '?did=', 'msg_information_saved', '');
		}
	}
	
	function slider() {
	$q = db_phquery("SELECT * FROM 1_slider ORDER BY sort_order");
	$data = array();
		while( $row = db_fetch_assoc($q) ){
			$data[] = $row;
		}
	return $data;
	}
	
	function main(){
		$Register = &Register::getInstance();
		/*@var $Register Register*/
		$smarty = &$Register->get(VAR_SMARTY);
		/*@var $smarty Smarty*/

		$list_sliders = self::slider(); //print_r($list_sliders);
		$smarty->assign('list_sliders', $list_sliders);
		$smarty->assign('admin_sub_dpt', 'slider.html');
	}
}

ActionsController::exec('SliderController');

/*echo '<pre>';
print_r ($_POST);
echo '</pre>';*/
?>