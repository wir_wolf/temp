<?php
define('CWORLDPAY_TTL', 
	'RBS WorldPay');
define('CWORLDPAY_DSCR', 
	'Обработка кредитных карт через платежную систему WorldPay (www.worldpay.com). Метод HTML Redirect');
	
define('CWORLDPAY_CFG_INSTID_TTL', 
	'WorldPay ID');
define('CWORLDPAY_CFG_INSTID_DSCR', 
	'Введите ваш идентификатор в системе WorldPay');

define('CWORLDPAY_CFG_TEST_TTL', 'Тестовый режим');
define('CWORLDPAY_CFG_TEST_DSCR', ' ');
	
define('CWORLDPAY_TXT_AFTER_PROCESSING_HTML_1', 
	'Перейти к оплате на сервере WorldPay!');
?>