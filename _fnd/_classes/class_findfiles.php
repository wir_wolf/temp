<?
class ListFoldersFiles{ 
 
 var $CodesFiles = "all";
 var $TypesFiles = "all";
 var $STRinFiles = "";
 var $CountFound = 0;
 var $List_Files = "";
 var $TotalSizes = 0; 

 function SetTypesFiles($FType){
  if($FType != "Все типы"){
   $FType = str_replace(" ", "",$FType);
   $FType = str_replace(",","|",$FType);
  }
  else $FType = "all";
  $this->TypesFiles = $FType;
 }
 
 function ReadDIR($Path){
  $d = dir($Path);  
  while(false !== ($entry = $d->read())){  
   if($entry!='.' && $entry!='..' && $entry!='' ){
    $all_path = $Path.$entry;
    $new_path = $this->go($all_path, is_file($all_path));  
    if(!is_file($all_path)){ if(!$this->ReadDIR($new_path)) return false; }
   }
  }  
  return true;
 } 
 
 function go($path2file, $is_file = true){  
  if($is_file){
   $found = 0; 
   if($this->TypesFiles == "all" || preg_match("/\.(".$this->TypesFiles.")$/i",$path2file)) $found = 1;
   if($found == 1 && $this->CodesFiles != "all") if(Get_File_KOD($path2file) != $this->CodesFiles) $found = 0;
   if($found == 1 && $this->STRinFiles != "") if(!Get_File_STR($path2file, $this->STRinFiles)) $found = 0;
   if($found == 1){	
	 $this->CountFound++;
	 if($this->CountFound == 1) $this->List_Files .= Get_DateTime()."\n-------------------\n"; 
	 $tmpFname = str_replace("../","",$path2file);
	 $this->List_Files .= $tmpFname."\n";
	 $this->TotalSizes = $this->TotalSizes + filesize($_SERVER['DOCUMENT_ROOT']."/".$tmpFname);
   }
   else $path2file = $path2file.'/';     
  }
  else $path2file = $path2file.'/';    
  return $path2file;
 } 
}
?>