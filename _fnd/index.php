<? $ver = "v1.2.1 UTF"; header("Content-Type:text/html; charset=utf-8"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru-ru" lang="ru-ru" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? $Title = "Поиск файлов"; ?>
<title><?=$Title." ".$ver;?></title>
<link href="_css/main.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/jscript">function qdstrct(){if(confirm("Удалить данное приложение с сервера?")) window.location="index.php?lqr=yes";}</script>
</head>
<body>
<? 
 define("WorkDIR", str_replace('\\','/', dirname(__FILE__)) ."/"); 
 include(WorkDIR."_incs/_common.php");
?> 
<div class="expanse">
 <div class="container shdw">
  <div class="title"><a class="redb" onclick="qdstrct(); return(false);" href="index.php?lqr=yes">Self-destruct ...&raquo;</a><?=$Title." ".$ver;?></div>
<form action="index.php" enctype="multipart/form-data" method="post">
<table class="mainform"><col class="firstcol" /><col />
<tr>
 <td colspan="2">Искать файлы с кодировкой:&nbsp;<select name="filecode">
 <?
 foreach($AllCODEs as $key => $val){
  $optstr = '<option';
  if($filecode == $key) $optstr .= ' selected="selected"';
  echo($optstr.' value="'.$key.'">'.$val.'</option>');  
 }
 ?>
 </select>
 </td>
</tr>
<tr><td>Место поиска:</td>
<td>
<? 
if(file_exists(WorkDIR."rootdirs.txt")){
 $filedirs = file_get_contents(WorkDIR."rootdirs.txt");	
 $RD = explode("\n", $filedirs);
 if(count($RD) > 1){
 echo('<select name="srchpth" style="width:100%;">');
  for($r=0; $r < count($RD); $r++){
   $RD[$r] = str_replace("\\","/",$RD[$r]);
   $SEL = ''; if($RD[$r] == $srchpth) $SEL = ' selected="selected"'; 
   echo('<option value="'.$RD[$r].'"'.$SEL.'>/'.$RD[$r].'</option>');
  }
  echo('</select>');
 }
} else { ?><input type="text"  name="srchpth" value="<?=$srchpth; ?>" /><? } ?>
</td>
</tr>
<tr><td width="100">Типы файлов:</td><td align="left"><input type="text" style="width:60%;" name="filetype" value="<?=$filetype;?>" />&nbsp;&nbsp;html, php, css, ...</td></tr>
<tr><td>С текстом:</td><td align="left"><input type="text" style="width:60%;" name="srchstr" value="<? echo(str_replace('"','&quot;', $srchstr)); ?>" />&nbsp;<input type="checkbox" name="regisr" <? if($regisr){ ?> checked="checked" <? } ?>/>&nbsp;Учитывать регистр</td></tr>
<tr><td><? if(isset($CountF) && $CountF > 0 || isset($Compare)){ ?>Сравнить:<? } else echo "&nbsp;"; ?></td><td>
<input type="hidden" name="getlistfiles" value="yes" />
<input style="float:right;"  type="submit" class="shdwcr" value="Выполнить" />
<? if(isset($CountF) && $CountF > 0 || isset($Compare)){ ?><input class="slimbrdal" type="file" name="compares" /> <? } ?>
</td></tr></table>
</form> 
<? if(isset($CountF)) include("_incs/showlistfiles.php"); ?>
  </div>
 </div>
</body>
</html>