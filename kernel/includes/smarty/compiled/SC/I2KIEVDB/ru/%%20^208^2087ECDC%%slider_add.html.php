<?php /* Smarty version 2.6.26, created on 2014-02-26 13:29:38
         compiled from backend/slider_add.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'set_query_html', 'backend/slider_add.html', 2, false),array('modifier', 'escape', 'backend/slider_add.html', 36, false),)), $this); ?>
<h1 class="breadcrumbs">
	<a href="<?php echo ((is_array($_tmp='?did=')) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
">Слайдер</a>
	&raquo;
	Новый слайд
</h1>

<?php echo $this->_tpl_vars['MessageBlock']; ?>


<form action='<?php echo ((is_array($_tmp="")) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
' method="post" enctype="multipart/form-data" name="slider_form">
<input name="action" value="save_method" type="hidden" />
<input name="type" value="add" type="hidden" />

<table cellspacing="0" cellpadding="5">
<!-- <tr>
	<td><?php echo 'Включен'; ?>
:</td>
	<td>
		<input type="checkbox" name="Enabled" <?php if ($this->_tpl_vars['shipping_method']['Enabled']): ?> checked<?php endif; ?> value="1" />
	</td>
</tr> -->
<tr>
	<td width="250"><?php echo 'Название'; ?>
:</td>
	<td>
		<input type="text" name="name" value="" style="width:250px;"/>
	</td>
</tr>
<tr>
	<td width="250"><?php echo 'Описание'; ?>
:</td>
	<td>
		<textarea rows="5" name="description" placeholder="Описание слайда" value="<?php echo $this->_tpl_vars['data']['description']; ?>
" style="width:250px;"><?php echo $this->_tpl_vars['data']['description']; ?>
</textarea>
	</td>
</tr>
<tr>
	<td valign="top" width="250">URL страницы (куда перейти):</td>
	<td>
		<input type="text" name="url" value="" style="width:250px;"/>
		<?php if ($this->_tpl_vars['shipping_method']['logo']): ?><br/><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['shipping_method']['logo'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" alt="" /><?php endif; ?>
	</td>
</tr>
<tr>
	<td valign="top" width="250">URL файла картинки:<br /> (начиная от папки с картинками <strong><?php echo @URL_IMAGES; ?>
/</strong> )</td>
	<td>
		<input type="text" name="url_image" value="" style="width:250px;"/> *
	</td>
</tr>

<tr>
	<td colspan="2">
		<input value="<?php echo 'Сохранить'; ?>
" type="submit" />
		<input value="<?php echo 'Отмена'; ?>
" type="button" onclick="document.location.href = '<?php echo ((is_array($_tmp='?did=')) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
';" />
	</td>
</tr>

</table>