<?php /* Smarty version 2.6.26, created on 2014-02-26 13:53:36
         compiled from backend/erase_products.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'translate', 'backend/erase_products.htm', 1, false),array('modifier', 'set_query_html', 'backend/erase_products.htm', 8, false),)), $this); ?>
<h1><?php echo ((is_array($_tmp=$this->_tpl_vars['CurrentDivision']['name'])) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>
</h1>

<?php if ($this->_tpl_vars['MessageBlock__success']): ?>
<?php echo $this->_tpl_vars['MessageBlock__success']; ?>

<?php else: ?>
<p><?php echo 'Здесь вы можете нажатием одной кнопки удалить все продукты, категории продуктов и их характеристики (действие необратимое).<br />Нажатие по кнопке не затронет списки заказов и покупателей вашего магазина, информационные страницы, блог и прочие настройки.'; ?>
</p>

<form action="<?php echo ((is_array($_tmp='')) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
" method="post">
<input name="action" value="erase_products" type="hidden" />
<input value="<?php echo 'Удалить'; ?>
" class="confirm_action" title="<?php echo 'Вы действительно хотите удалить все продукты? Действие необратимо.'; ?>
" style="font-size: 120%;" type="submit" />
</form>
<?php endif; ?>