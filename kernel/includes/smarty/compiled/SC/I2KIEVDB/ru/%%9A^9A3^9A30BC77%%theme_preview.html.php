<?php /* Smarty version 2.6.26, created on 2014-02-26 12:09:25
         compiled from theme_preview.html */ ?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<base href="<?php echo @CONF_FULL_SHOP_URL; ?>
" />
<!-- Head start -->
		<?php echo $this->_tpl_vars['tpl_head']; ?>

<!-- Head end -->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['URL_THEME_OFFSET']; ?>
/head.js"></script>
		<link rel="stylesheet" href="<?php echo @URL_CSS; ?>
/general.css" type="text/css" />
		<?php if ($this->_tpl_vars['overridestyles']): ?><link rel="stylesheet" href="<?php echo $this->_tpl_vars['URL_THEME_OFFSET']; ?>
/overridestyles.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo $this->_tpl_vars['URL_THEME_OFFSET']; ?>
/main.css" type="text/css" />
		<?php else: ?>
		
		<link rel="stylesheet" href="<?php echo $this->_tpl_vars['tpl_css_path']; ?>
/overridestyles.css" type="text/css" />	
		<link rel="stylesheet" href="<?php echo $this->_tpl_vars['tpl_css_path']; ?>
/main.css" type="text/css" />	
		<?php endif; ?>
		
		
		<script type="text/javascript" src="<?php echo @URL_JS; ?>
/functions.js"></script>
		<script type="text/javascript" src="<?php echo @URL_JS; ?>
/behavior.js"></script>
	</head>
	<body>
	<?php echo $this->_tpl_vars['tpl_content']; ?>

	<div style="position:fixed;color:transparent;width:100%;height:100%;z-index:9999;left:0;top:0;right:0;bottom:0;">&nbsp;</div>
	</body>
</html>