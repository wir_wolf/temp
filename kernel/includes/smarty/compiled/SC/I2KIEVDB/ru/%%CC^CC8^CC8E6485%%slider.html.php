<?php /* Smarty version 2.6.26, created on 2014-02-26 13:29:29
         compiled from backend/slider.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'translate', 'backend/slider.html', 14, false),array('modifier', 'set_query_html', 'backend/slider.html', 21, false),)), $this); ?>
<link rel="stylesheet" href="<?php echo @URL_ROOT; ?>
/3rdparty/highslide/highslide.css" type="text/css" />
<script type="text/javascript" src="<?php echo @URL_ROOT; ?>
/3rdparty/highslide/highslide.js"></script>
<script type="text/javascript">    
    hs.graphicsDir = '<?php echo @URL_ROOT; ?>
/3rdparty/highslide/graphics/';
    hs.outlineType = 'rounded-white';
</script>
<style type="text/css">  
    <?php echo '  
        #tbl-slider td {padding: 5px;min-width: 50px;}
        #tbl-slider .dragable tr:hover td {background-color: #dfdfdf; } 
    '; ?>

</style>

<h1><?php echo ((is_array($_tmp=$this->_tpl_vars['CurrentDivision']['name'])) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>
</h1>
<?php echo $this->_tpl_vars['MessageBlock']; ?>

<?php if ($this->_tpl_vars['MessageBlock__success']): 
 echo $this->_tpl_vars['MessageBlock__success']; ?>

<?php else: ?>
<p></p>

<form action="<?php echo ((is_array($_tmp='')) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
" class="ajaxform" method="post" enctype="multipart/form-data">
<input name="action" value="save_order" type="hidden" />
<table id="tbl-slider">
<hr /><b>Уже добавленные изображения</b>
<tr bgcolor='#555' align='center'>
	<td><font size=2 color='#ffffff'><b>Изображение</b></font></td>
	<td><font size=2 color='#ffffff'><b>Название</b></font></td>
	<td><font size=2 color='#ffffff'><b>URL страницы</b></font></td>
	<td><font size=2 color='#ffffff'><b>URL картинки (начиная от <?php echo @URL_IMAGES; ?>
/)</b></font></td>
	<td><font size=2 color='#ffffff'><b>Описание</b></font></td>
	<td></td>
	<td><font size=2 color='#ffffff'><b>Удалить</b></font></td>
</tr>
<?php $_from = $this->_tpl_vars['list_sliders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['_fe'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['_fe']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['_method']):
        $this->_foreach['_fe']['iteration']++;
?>
<tbody class="dragable">
<tr bgcolor='#ededed' align="center">

	<td>
		<a class='highslide' href='<?php echo @URL_IMAGES; ?>
/<?php echo $this->_tpl_vars['_method']['url_image']; ?>
' onclick="return hs.expand(this, { slideshowGroup: <?php echo $this->_tpl_vars['_method']['id']; ?>
})">
			<img style="max-height: 200px;" src='<?php echo @URL_IMAGES; ?>
/<?php echo $this->_tpl_vars['_method']['url_image']; ?>
' width='100' alt='' />
		</a>
	</td>

	<td class="handle"><span><?php echo $this->_tpl_vars['_method']['name']; ?>
</span>
		<input type="hidden" class="field_priority" name="priority_<?php echo $this->_tpl_vars['_method']['id']; ?>
" value="<?php echo ($this->_foreach['_fe']['iteration']-1); ?>
" />
	</td>

	<td class="handle">
		<?php echo $this->_tpl_vars['_method']['url']; ?>

	</td>

	<td class="handle">
		<?php echo $this->_tpl_vars['_method']['url_image']; ?>

	</td>
	
	<td class="handle">
		<?php echo $this->_tpl_vars['_method']['description']; ?>

	</td>

	<td>
		<a href='<?php echo ((is_array($_tmp="action=edit_method&sid=".($this->_tpl_vars['_method']['id']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
'><?php echo 'Редактировать'; ?>
</a>
	</td>

	<td>
		<a href="javascript:confirmDelete('','Удалить слайд?','/published/SC/html/scripts/index.php?did=216&action=delete_method&sid=<?php echo $this->_tpl_vars['_method']['id']; ?>
');">
		<img alt="Удалить" src="images/remove.gif" border="0"></a>
	</td>
</tr>
</tbody>
<?php endforeach; endif; unset($_from); 
 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "backend/sortable_table.html", 'smarty_include_vars' => array('table_id' => "tbl-slider")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-- <tr>
<td><input type="radio" name="price_manipulation" id="price_manipulation1" value="multiplication_plus"><label for="price_manipulation1"> Добавить % </label></td>
<td><label for="price_manipulation1">+<input type="text" name="price1" size="10">EURO</label></td>
</tr>

<tr>
<td><input type="radio" name="price_manipulation" id="price_manipulation2" value="multiplication_minus"><label for="price_manipulation2"> Отнять % </label></td>
<td><label for="price_manipulation2">−<input type="text" name="price2" size="10">EURO</label></td>
</tr>

<tr>
<td><input type="radio" name="price_manipulation" id="price_manipulation3" value="addition"><label for="price_manipulation3"> Добавить к цене </label></td>
<td><label for="price_manipulation3">+<input type="text" name="price3" size="10">EURO</label></td>
</tr>

<tr>
<td><input type="radio" name="price_manipulation" id="price_manipulation4" value="subtraction"><label for="price_manipulation4"> Отнять от цены </label></td>
<td><label for="price_manipulation4">−<input type="text" name="price4" size="10">EURO</label></td>
</tr>

<tr><td><input value="Изменить цену" class="confirm_action" title="Изменить цену" style="font-size: 120%;" type="submit" /></td></tr> -->
</table>
</form>
<input value="Добавить слайд" onclick="document.location.href='<?php echo ((is_array($_tmp="action=add_method")) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
'" type="button" />
<?php endif; ?>