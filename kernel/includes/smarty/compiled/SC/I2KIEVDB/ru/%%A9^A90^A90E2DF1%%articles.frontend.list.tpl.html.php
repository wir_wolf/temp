<?php /* Smarty version 2.6.26, created on 2014-02-25 16:20:54
         compiled from articles.frontend.list.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'set_query', 'articles.frontend.list.tpl.html', 6, false),array('modifier', 'set_query_html', 'articles.frontend.list.tpl.html', 7, false),array('modifier', 'escape', 'articles.frontend.list.tpl.html', 10, false),array('modifier', 'default', 'articles.frontend.list.tpl.html', 10, false),array('modifier', 'translate', 'articles.frontend.list.tpl.html', 22, false),array('modifier', 'date_format', 'articles.frontend.list.tpl.html', 66, false),array('modifier', 'replace', 'articles.frontend.list.tpl.html', 66, false),)), $this); ?>
<div class="clearfix" id="cat_path">
<table cellpadding="0" border="0" class="cat_path_in_productpage">
	<tr>
	<td>
		<a href="<?php echo ((is_array($_tmp="?")) ? $this->_run_mod_handler('set_query', true, $_tmp) : smarty_modifier_set_query($_tmp)); ?>
"><?php echo 'Главная'; ?>
</a>&nbsp;<?php echo $this->_tpl_vars['BREADCRUMB_DELIMITER']; ?>

		<a href='<?php echo ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
'><?php echo 'Статьи'; ?>
</a>
		<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['product_category_path']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
			<?php if ($this->_tpl_vars['product_category_path'][$this->_sections['i']['index']]['CategoryID']): ?>
				<a href='<?php echo ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL)."&CategoryID=".($this->_tpl_vars['product_category_path'][$this->_sections['i']['index']]['CategoryID'])."&CategorySlug=".($this->_tpl_vars['product_category_path'][$this->_sections['i']['index']]['CategorySlug']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
'><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['product_category_path'][$this->_sections['i']['index']]['CategoryTitle'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('default', true, $_tmp, "(no name)") : smarty_modifier_default($_tmp, "(no name)")); ?>
</a>
			<?php endif; ?>
			<?php if (! $this->_sections['i']['last']): ?>&nbsp;<?php echo $this->_tpl_vars['BREADCRUMB_DELIMITER']; 
 endif; ?>
		<?php endfor; endif; ?>
	</td>
	</tr>
</table>
</div>
<br>

<h1 class="ArticleList-category-title">
	<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['Category']['CategoryTitle'])) ? $this->_run_mod_handler('default', true, $_tmp, 'pgn_articles') : smarty_modifier_default($_tmp, 'pgn_articles')))) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>

	<a href="<?php echo ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL)."&rss=rss")) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
"><img src="<?php echo @URL_IMAGES_COMMON; ?>
/rss-feed.png" alt="RSS 2.0"  style="padding-left:10px;"></a>
</h1>


<div class="clearfix" id="cat_top_tree">
	<div >
		
		<?php if ($this->_tpl_vars['Category']['CategoryDefaultPicture']): ?>
			<img src="<?php echo @URL_ARTICLES_PICTURES; ?>
/<?php echo ((is_array($_tmp=$this->_tpl_vars['Category']['CategoryDefaultPicture'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
" alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['Category']['CategoryTitle'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['Category']['CategoryTitle'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
"  align="left" style="margin:0 10" class="ArticleList-category-img">
		<?php endif; ?>
			
		<div class="ArticleList-category-desc"><?php echo $this->_tpl_vars['Category']['CategoryDescription']; ?>
</div>
		
	
		<?php if ($this->_tpl_vars['Categorys']): ?>
		<br>
		<div class="ArticleList-subcategories">
			<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['Categorys']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
				<?php if ($this->_tpl_vars['Categorys'][$this->_sections['i']['index']]['CategorySlug']): ?>
					<?php $this->assign('_sub_category_url', ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL)."&CategoryID=".($this->_tpl_vars['Categorys'][$this->_sections['i']['index']]['CategoryID'])."&CategorySlug=".($this->_tpl_vars['Categorys'][$this->_sections['i']['index']]['CategorySlug']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp))); ?>
				<?php else: ?>
					<?php $this->assign('_sub_category_url', ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL)."&CategoryID=".($this->_tpl_vars['Categorys'][$this->_sections['i']['index']]['CategoryID']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp))); ?>
				<?php endif; ?>
				<a href="<?php echo $this->_tpl_vars['_sub_category_url']; ?>
"><?php echo $this->_tpl_vars['Categorys'][$this->_sections['i']['index']]['CategoryTitle']; ?>
</a>(<?php echo $this->_tpl_vars['Categorys'][$this->_sections['i']['index']]['CountArticles']; ?>
)<br>
			<?php endfor; endif; ?>
		</div>
		<?php endif; ?>
	</div>

</div>


	
<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['Articles']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
	<div class="post_block ArticleList-articles">
		
		
		<h2 class="post_title ArticleList-articles-title">
			<a href="<?php echo ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL)."&CategoryID=".($this->_tpl_vars['Articles'][$this->_sections['i']['index']]['CategoryID'])."&CategorySlug=".($this->_tpl_vars['Articles'][$this->_sections['i']['index']]['CategorySlug'])."&postID=".($this->_tpl_vars['Articles'][$this->_sections['i']['index']]['ArticleID'])."&postSlug=".($this->_tpl_vars['Articles'][$this->_sections['i']['index']]['ArticleSlug']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
"><?php echo $this->_tpl_vars['Articles'][$this->_sections['i']['index']]['ArticleTitle']; ?>
</a>
		</h2>

		<div class="post_date ArticleList-articles-date">
			<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['Articles'][$this->_sections['i']['index']]['ArticleDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'January', "Января") : smarty_modifier_replace($_tmp, 'January', "Января")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'February', "Февраля") : smarty_modifier_replace($_tmp, 'February', "Февраля")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'March', "Марта") : smarty_modifier_replace($_tmp, 'March', "Марта")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'April', "Апреля") : smarty_modifier_replace($_tmp, 'April', "Апреля")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'May', "Мая") : smarty_modifier_replace($_tmp, 'May', "Мая")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'June', "Июня") : smarty_modifier_replace($_tmp, 'June', "Июня")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'July', "Июля") : smarty_modifier_replace($_tmp, 'July', "Июля")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'August', "Августа") : smarty_modifier_replace($_tmp, 'August', "Августа")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'September', "Сентября") : smarty_modifier_replace($_tmp, 'September', "Сентября")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'October', "Октября") : smarty_modifier_replace($_tmp, 'October', "Октября")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'November', "Ноября") : smarty_modifier_replace($_tmp, 'November', "Ноября")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'December', "Декабря") : smarty_modifier_replace($_tmp, 'December', "Декабря")); ?>

		</div>
		
		<div class="post_content ArticleList-articles-desc">
			<table cellpadding=0 cellspacing=0 border=0 width=100%><tr><td>
	
			<?php if ($this->_tpl_vars['Articles'][$this->_sections['i']['index']]['ArticleDefaultSmallPicture']): ?>
				<a href="<?php echo ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL)."&CategoryID=".($this->_tpl_vars['Articles'][$this->_sections['i']['index']]['CategoryID'])."&CategorySlug=".($this->_tpl_vars['Articles'][$this->_sections['i']['index']]['CategorySlug'])."&postID=".($this->_tpl_vars['Articles'][$this->_sections['i']['index']]['ArticleID'])."&postSlug=".($this->_tpl_vars['Articles'][$this->_sections['i']['index']]['ArticleSlug']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
">
				<img alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['Articles'][$this->_sections['i']['index']]['ArticleTitle'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" src="<?php echo @URL_ARTICLES_PICTURES; ?>
/<?php echo ((is_array($_tmp=$this->_tpl_vars['Articles'][$this->_sections['i']['index']]['ArticleDefaultSmallPicture'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
" align="left" style="margin:10" class="ArticleList-articles-img"/>
				</a>
			<?php endif; ?>
		
			<?php echo $this->_tpl_vars['Articles'][$this->_sections['i']['index']]['ArticleBriefDescription']; ?>

			</td></tr></table>
		</div>
	</div>
<?php endfor; else: ?>
	<center><?php echo 'пустой список'; ?>
</center>
<?php endif; ?>

	
<?php if ($this->_tpl_vars['NavigatorHtml']): ?><br><br><div><?php echo $this->_tpl_vars['NavigatorHtml']; ?>
</div><?php endif; ?>