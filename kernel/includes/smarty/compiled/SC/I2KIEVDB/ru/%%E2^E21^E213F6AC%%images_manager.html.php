<?php /* Smarty version 2.6.26, created on 2014-02-26 12:07:40
         compiled from backend/images_manager.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'translate', 'backend/images_manager.html', 5, false),array('modifier', 'set_query_html', 'backend/images_manager.html', 12, false),array('modifier', 'replace', 'backend/images_manager.html', 35, false),array('modifier', 'escape', 'backend/images_manager.html', 94, false),array('modifier', 'set_query', 'backend/images_manager.html', 167, false),array('function', 'cycle', 'backend/images_manager.html', 113, false),)), $this); ?>
<script type="text/javascript" src="<?php echo @URL_JS; ?>
/niftycube.js"></script>



<h1><?php echo ((is_array($_tmp=$this->_tpl_vars['CurrentDivision']['name'])) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>
</h1>
<p><?php echo 'В этом разделе вы можете загрузить изображения для вашего интернет-магазина.<br />
Поддерживается возможность загрузки следующих типов изображений: GIF, JPG, BMP, PNG.'; ?>
</p>

<?php echo $this->_tpl_vars['MessageBlock']; ?>


<ul id="edmod">
	<li class="tab<?php if ($this->_tpl_vars['folder_id'] == 'images'): ?> current<?php endif; ?>"><a href='<?php echo ((is_array($_tmp="page=&folder_id=images")) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
'><?php echo 'Картинки'; ?>
</a></li>
	<li class="tab<?php if ($this->_tpl_vars['folder_id'] == 'prdpicts'): ?> current<?php endif; ?>"><a href='<?php echo ((is_array($_tmp="page=&folder_id=prdpicts")) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
'><?php echo 'Картинки продуктов'; ?>
</a></li>
</ul>

<script type="text/javascript">
<!--
var another_type = 'upload';
//-->
</script>


<span style="float: right;">
<?php echo 'Вид'; ?>
:&nbsp;
<?php if ($this->_tpl_vars['view_mode'] == 'thumbnails'): ?>
<a href="<?php echo ((is_array($_tmp="view_mode=list")) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
" title="<?php echo 'списком'; ?>
"><?php echo 'списком'; ?>
</a>
|
<span style="font-weight:bolder;" title="<?php echo 'эскизами'; ?>
"><?php echo 'эскизами'; ?>
</span>
<?php else: ?>
<span style="font-weight:bolder;" title="<?php echo 'списком'; ?>
"><?php echo 'списком'; ?>
</span>
|
<a href="<?php echo ((is_array($_tmp="view_mode=thumbnails")) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
" title="<?php echo 'эскизами'; ?>
"><?php echo 'эскизами'; ?>
</a>
<?php endif; ?>
</span>
<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['folder']['description'])) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)))) ? $this->_run_mod_handler('replace', true, $_tmp, '[url]', ($this->_tpl_vars['folder']['url'])."/") : smarty_modifier_replace($_tmp, '[url]', ($this->_tpl_vars['folder']['url'])."/")); ?>


<div style="display:block;margin-top:40px;width:500px;">
	<div class="imm_upload_swf_link_block" style="display:none;margin:0;padding:0">
		<a style="padding:10px;" href="#swfupload_image" id ="tab-swfupload-link"><?php echo 'Загрузить много изображений'; ?>
</a>
	</div>
	<a style="padding:10px;" href="#upload_image" id="tab-upload-link" style="background-color: #f5f0bb;"><?php echo 'Загрузить по одному'; ?>
</a>
	<?php if ($this->_tpl_vars['folder_id'] == 'prdpicts' && $this->_tpl_vars['images_list']): ?>
	<a style="padding:10px;" href='#deleteall_images' id="tab-deleteall-link" title='<?php echo 'Вы действительно хотите удалить отмеченные изображения?'; ?>
'><?php echo 'Удалить все'; ?>
</a>
	<?php endif; ?>

	<div id="tab-swfupload-section" style="display:block;background-color: #f5f0bb;padding:10px;margin:10px 0;min-height: 80px;">
		<div id="divSWFUploadUI">
			<div class="fieldset  flash" id="fsUploadProgress" style="display:none;width:450px;max-height:300px;overflow: auto;">
											</div>
			<p>
				<span id="spanButtonPlaceholder"></span>
			</p>
		
		</div>
	</div>
	
	<div id="tab-upload-section" style="display:none;background-color: #f5f0bb;padding:10px;margin:10px 0;min-height: 80px;">
	<form class="imm_upload_form" method="post" action="<?php echo ((is_array($_tmp='action=upload_image')) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
" enctype="multipart/form-data">
		<input name="upload_picture" type="file" />
		<input value="<?php echo 'Загрузить'; ?>
" type="submit"/>
		</form>
	</div>
	<?php if ($this->_tpl_vars['folder_id'] == 'prdpicts' && $this->_tpl_vars['images_list']): ?>
	<div id="tab-deleteall-section" style="display:none;background-color: #f5f0bb;padding:10px;margin:10px 0;min-height: 80px;">
	<form class="imm_upload_form" method="post" action="<?php echo ((is_array($_tmp='action=delete_all')) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
" enctype="multipart/form-data">
		<br />
		<br />
		<input value="<?php echo 'Удалить все'; ?>
" class="confirm_action" title="<?php echo 'Вы действительно хотите удалить отмеченные изображения?'; ?>
" type="submit"/>
		</form>
	</div>
	<?php endif; ?>

</div>		


<div id="divStatus" class="success_block" style="display:none" ></div>
<?php if ($this->_tpl_vars['images_list']): ?>
<?php ob_start(); ?>
<nobr><span style="margin-right:20px;"><?php echo $this->_tpl_vars['images_list_info']['string']; ?>
</span><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "backend/lister.tpl.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></nobr>
<?php $this->_smarty_vars['capture']['page_navigator'] = ob_get_contents(); ob_end_clean(); ?>
<?php echo $this->_smarty_vars['capture']['page_navigator']; ?>

<form action="<?php echo ((is_array($_tmp="")) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
" id="image_list" method="post">
<ul id="imm-images-box" style="clear:both;">
<?php if ($this->_tpl_vars['view_mode'] == 'thumbnails'): ?>
<table>
<tr><td>
<?php $_from = $this->_tpl_vars['images_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['frimlist'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['frimlist']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['_image']):
        $this->_foreach['frimlist']['iteration']++;
?>
	<li class="imm_img_blck">
		<table align="center">
		<tr>
			<td height="110" width="110" align="center" valign="middle" class="imm_img_box">
				<img align="middle" valign="middle" src="<?php echo ((is_array($_tmp=$this->_tpl_vars['_image']['url'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['_image']['file'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" width="<?php echo $this->_tpl_vars['_image']['thumb_width']; ?>
" height="<?php echo $this->_tpl_vars['_image']['thumb_height']; ?>
" img_width="<?php echo $this->_tpl_vars['_image']['width']; ?>
" img_height="<?php echo $this->_tpl_vars['_image']['height']; ?>
" img_id="<?php echo $this->_tpl_vars['_image']['id']; ?>
" img_file="<?php echo ((is_array($_tmp=$this->_tpl_vars['_image']['file'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" />
			</td>
		</tr>
		</table>
	</li>
<?php endforeach; endif; unset($_from); ?>
</td></tr>
</table>
</ul>

<?php else: ?>
<table class="grid" width="100%">
<tr class="gridsheader">
	<td width="5%">				&nbsp;								</td>
	<td colspan="3" width="50%"><?php echo 'Фотография'; ?>
</td>
	<td colspan="1" width="15%"><?php echo 'Изменен'; ?>
		</td>
	<td width="30%">			<?php echo 'Постоянная ссылка'; ?>
			</td>
</tr>
<?php $_from = $this->_tpl_vars['images_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['frimlist'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['frimlist']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['_image']):
        $this->_foreach['frimlist']['iteration']++;
?>
<tr class="<?php echo smarty_function_cycle(array('values' => "gridline1,gridline"), $this);?>
">
	<td>
		<input type="checkbox" class="checkbox select_images" rel="group-box" name="image_<?php echo $this->_tpl_vars['_image']['id']; ?>
" id="image_<?php echo $this->_tpl_vars['_image']['id']; ?>
" value="1">
	</td>
	<td><a class="imm_img_view bluehref" href="<?php echo ((is_array($_tmp=$this->_tpl_vars['_image']['url'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['_image']['file'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" target="_blank" img_width="<?php echo $this->_tpl_vars['_image']['width']; ?>
" img_height="<?php echo $this->_tpl_vars['_image']['height']; ?>
"><?php echo $this->_tpl_vars['_image']['file']; ?>
</a></td>
	<td><?php echo $this->_tpl_vars['_image']['width']; ?>
&times;<?php echo $this->_tpl_vars['_image']['height']; ?>
&nbsp;px</td>
	<td><?php echo $this->_tpl_vars['_image']['size']; ?>
</td>	
	<td><?php echo $this->_tpl_vars['_image']['mtime']; ?>
</td>
	<td><input style="width:400px;" onclick="this.select();" onfocus="this.select();" readonly="readonly" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['_image']['full_url'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
"></td>
</tr>
<?php endforeach; endif; unset($_from); ?>
<tr class="gridsfooter">
	<td><input type="checkbox" rel="select_images" id="group-box" class="groupcheckbox"/></td>
	<td colspan="5"><?php if ($this->_tpl_vars['images_list']): ?>
	<?php echo 'С отмеченными:'; ?>
&nbsp;
	<input type="hidden" name="action" value="delete_image">
	<input type="submit" class="confirm_action" title="<?php echo 'Вы действительно хотите удалить отмеченные изображения?'; ?>
" value="<?php echo 'Удалить'; ?>
">
	<?php else: ?>&nbsp;<?php endif; ?>
	</td>
</tr>
</table>
<?php endif; ?>
</form>
<?php echo $this->_smarty_vars['capture']['page_navigator']; ?>

<br />
<br />


<?php else: ?>

<p><?php echo 'В этой папке нет изображений'; ?>
</p>
<?php endif; ?>

<div style="display: none;" id="blck-img-menu"><div id="blck-img-menu-sub">
<div id="imm-img-file"></div>
<a href="#view" id="imm-img-view"><?php echo 'Просмотр'; ?>
</a><br />
<a href="#permalink" id="imm-img-permalink"><?php echo 'Постоянная ссылка'; ?>
</a><span id="imm-img-permalink-field-box">
	<textarea id="imm-img-permalink-field" rows="3" cols="30" onclick="this.select();" onfocus="this.select();" readonly="readonly"></textarea>
</span>
<div>
<a href='<?php echo ((is_array($_tmp="action=delete_image&img_id=")) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
' id="imm-img-delete" title="<?php echo 'Удалить файл с сервера?'; ?>
"><?php echo 'Удалить'; ?>
</a>
</div>
</div></div>

<link rel="stylesheet" href="../../../common/html/res/swfupload/swfupload.css" type="text/css"/>

<script type="text/javascript" src="<?php echo @URL_3RDPARTY; ?>
/swfupload/swfupload.js"></script>
<script type="text/javascript" src="<?php echo @URL_3RDPARTY; ?>
/swfupload/js/swfupload.swfobject.js"></script>
<script type="text/javascript" src="<?php echo @URL_3RDPARTY; ?>
/swfupload/js/swfupload.queue.js"></script>
<script type="text/javascript" src="<?php echo @URL_3RDPARTY; ?>
/swfupload/js/fileprogress.js"></script>
<script type="text/javascript" src="<?php echo @URL_3RDPARTY; ?>
/swfupload/js/handlers.js"></script>


<script type="text/javascript">
var phploadurl = "<?php echo ((is_array($_tmp='?ukey=&did=&folder_id=')) ? $this->_run_mod_handler('set_query', true, $_tmp) : smarty_modifier_set_query($_tmp)); ?>
";
var swfurl = "<?php echo @URL_3RDPARTY; ?>
/swfupload/swfupload.swf";
var swfbtn = "<?php echo @URL_3RDPARTY; ?>
/swfupload/61x22.png";
var refreshString = '  <a href="<?php echo ((is_array($_tmp='')) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
"><?php echo 'Обновить'; ?>
</a>';
var session_id = "<?php echo $this->_tpl_vars['session_id']; ?>
";
</script>


<script type="text/javascript" src="<?php echo @URL_JS; ?>
/images_manager.js"></script>