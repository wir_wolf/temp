<?php /* Smarty version 2.6.26, created on 2014-02-26 13:08:15
         compiled from article.frontend.post.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'set_query', 'article.frontend.post.tpl.html', 6, false),array('modifier', 'set_query_html', 'article.frontend.post.tpl.html', 7, false),array('modifier', 'escape', 'article.frontend.post.tpl.html', 10, false),array('modifier', 'default', 'article.frontend.post.tpl.html', 10, false),array('modifier', 'translate', 'article.frontend.post.tpl.html', 24, false),array('modifier', 'date_format', 'article.frontend.post.tpl.html', 43, false),array('modifier', 'replace', 'article.frontend.post.tpl.html', 43, false),array('modifier', 'linewrap', 'article.frontend.post.tpl.html', 167, false),array('modifier', 'nl2br', 'article.frontend.post.tpl.html', 169, false),)), $this); ?>
<div class="clearfix" id="cat_path">
<table cellpadding="0" border="0" class="cat_path_in_productpage">
	<tr>
	<td>
		<a href="<?php echo ((is_array($_tmp="?")) ? $this->_run_mod_handler('set_query', true, $_tmp) : smarty_modifier_set_query($_tmp)); ?>
"><?php echo 'Главная'; ?>
</a>&nbsp;<?php echo $this->_tpl_vars['BREADCRUMB_DELIMITER']; ?>

		<a href='<?php echo ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
'><?php echo 'Статьи'; ?>
</a>
		<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['product_category_path']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
			<?php if ($this->_tpl_vars['product_category_path'][$this->_sections['i']['index']]['CategoryID']): ?>
				<a href='<?php echo ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL)."&CategoryID=".($this->_tpl_vars['product_category_path'][$this->_sections['i']['index']]['CategoryID'])."&CategorySlug=".($this->_tpl_vars['product_category_path'][$this->_sections['i']['index']]['CategorySlug']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
'><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['product_category_path'][$this->_sections['i']['index']]['CategoryTitle'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('default', true, $_tmp, "(no name)") : smarty_modifier_default($_tmp, "(no name)")); ?>
</a>
			<?php endif; ?>
			<?php if (! $this->_sections['i']['last']): ?>&nbsp;<?php echo $this->_tpl_vars['BREADCRUMB_DELIMITER']; 
 endif; ?>
		<?php endfor; endif; ?>
	</td>
	</tr>
</table>
</div>
<br>



<h1 class="ArticlePost-title">
	<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['Article']['ArticleTitle'])) ? $this->_run_mod_handler('default', true, $_tmp, 'pgn_articles') : smarty_modifier_default($_tmp, 'pgn_articles')))) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>

	&nbsp;<a href="<?php echo ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL)."&rss=rss")) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
"><img src="<?php echo @URL_IMAGES_COMMON; ?>
/rss-feed.png" alt="RSS 2.0"  style="padding-left:10px;"></a>
</h1>



<div style="float:right">
	<?php if ($this->_tpl_vars['ArticleNextAndPrevoius']['Previous']): ?>
		<a href='<?php echo ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL)."&CategoryID=".($this->_tpl_vars['ArticleNextAndPrevoius']['Previous']['CategoryID'])."&CategorySlug=".($this->_tpl_vars['ArticleNextAndPrevoius']['Previous']['CategorySlug'])."&postID=".($this->_tpl_vars['ArticleNextAndPrevoius']['Previous']['ArticleID'])."&postSlug=".($this->_tpl_vars['ArticleNextAndPrevoius']['Previous']['ArticleSlug']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
' class="ArticlePost-previous"><?php echo 'Предыдущая запись'; ?>
</a>
	<?php endif; ?>
	&nbsp;&nbsp;&nbsp;
	<?php if ($this->_tpl_vars['ArticleNextAndPrevoius']['Next']): ?>
		<a href='<?php echo ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL)."&CategoryID=".($this->_tpl_vars['ArticleNextAndPrevoius']['Next']['CategoryID'])."&CategorySlug=".($this->_tpl_vars['ArticleNextAndPrevoius']['Next']['CategorySlug'])."&postID=".($this->_tpl_vars['ArticleNextAndPrevoius']['Next']['ArticleID'])."&postSlug=".($this->_tpl_vars['ArticleNextAndPrevoius']['Next']['ArticleSlug']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
' class="ArticlePost-next"><?php echo 'Следующая запись'; ?>
</a>
	<?php endif; ?>
</div>

	
<div class="post_date ArticlePost-date">
	<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['Article']['ArticleDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %B %Y") : smarty_modifier_date_format($_tmp, "%e %B %Y")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'January', "Января") : smarty_modifier_replace($_tmp, 'January', "Января")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'February', "Февраля") : smarty_modifier_replace($_tmp, 'February', "Февраля")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'March', "Марта") : smarty_modifier_replace($_tmp, 'March', "Марта")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'April', "Апреля") : smarty_modifier_replace($_tmp, 'April', "Апреля")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'May', "Мая") : smarty_modifier_replace($_tmp, 'May', "Мая")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'June', "Июня") : smarty_modifier_replace($_tmp, 'June', "Июня")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'July', "Июля") : smarty_modifier_replace($_tmp, 'July', "Июля")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'August', "Августа") : smarty_modifier_replace($_tmp, 'August', "Августа")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'September', "Сентября") : smarty_modifier_replace($_tmp, 'September', "Сентября")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'October', "Октября") : smarty_modifier_replace($_tmp, 'October', "Октября")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'November', "Ноября") : smarty_modifier_replace($_tmp, 'November', "Ноября")))) ? $this->_run_mod_handler('replace', true, $_tmp, 'December', "Декабря") : smarty_modifier_replace($_tmp, 'December', "Декабря")); ?>

</div>



<div class="post_block" style="padding:10px">
<table cellpadding=0 cellspacing=0 border=0 width=100%>
	<tr>
		<td>
			<div style="float:left;">
				
				<?php if ($this->_tpl_vars['Article']['ArticleDefaultBigPicture']): ?>
					<img alt="<?php echo ((is_array($_tmp=$this->_tpl_vars['Article']['ArticleTitle'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
" src="<?php echo @URL_ARTICLES_PICTURES; ?>
/<?php echo ((is_array($_tmp=$this->_tpl_vars['Article']['ArticleDefaultBigPicture'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
"style="margin:0px 10px;" align=top valign=left alt="" class="ArticlePost-image"/>
				<?php endif; ?>

				
				<?php if (@CONF_ARTCLE_YANDEX_SHARE == 'True'): ?>
				<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
				<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="<?php echo @CONF_ARTCLE_YANDEX_SHARE_LINK_TYPE; ?>
" data-yashareQuickServices="<?php echo @CONF_ARTCLE_YANDEX_SHARE_SERVICES; ?>
"></div> 
				<?php endif; ?>

			</div>
			<div class="post_content ArticlePost-text">	
				
				<?php echo $this->_tpl_vars['Article']['ArticleDescription']; ?>

			</div>
				
			<div style="text-align:right; font-weight:bold;" class="ArticlePost-author"><?php echo $this->_tpl_vars['Article']['ArticleAuthor']; ?>
</div>	
		</td>
	</tr>
</table>	
</div>


	
<?php if ($this->_tpl_vars['Article']['ArticleProducts']): ?>
<h2 class="ArticlePost-caption"><?php echo 'Рекомендуемые товары'; ?>
</h2>
	<div class="post_block" style="padding:10px">
	<table cellpadding="0" border="0" cellspacing=15  width=100%>
		<?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['Article']['ArticleProducts']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>
		<?php if ($this->_sections['j']['index']%4 == 0): ?></tr><tr><?php endif; ?>
			<td valign="top" align=center>
				
				<?php if ($this->_tpl_vars['Article']['ArticleProducts'][$this->_sections['j']['index']]['picture']['thumbnail']): ?>
					<div class="ArticlePost-product-image">
						<a href="<?php echo ((is_array($_tmp="?productID=".($this->_tpl_vars['Article']['ArticleProducts'][$this->_sections['j']['index']]['productID'])."&product_slug=".($this->_tpl_vars['Article']['ArticleProducts'][$this->_sections['j']['index']]['slug']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
">
							<img src="<?php echo @URL_PRODUCTS_PICTURES; ?>
/<?php echo $this->_tpl_vars['Article']['ArticleProducts'][$this->_sections['j']['index']]['picture']['thumbnail']; ?>
" alt="<?php echo $this->_tpl_vars['Article']['ArticleProducts'][$this->_sections['j']['index']]['name_ru']; ?>
">
						</a>
					</div>
				<?php endif; ?>
				<div >
				
					<a href="<?php echo ((is_array($_tmp="?productID=".($this->_tpl_vars['Article']['ArticleProducts'][$this->_sections['j']['index']]['productID'])."&product_slug=".($this->_tpl_vars['Article']['ArticleProducts'][$this->_sections['j']['index']]['slug']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
" class="ArticlePost-product-url"><?php echo $this->_tpl_vars['Article']['ArticleProducts'][$this->_sections['j']['index']]['name_ru']; ?>
</a>
				</div>				
				<table cellpadding=0 cellspacing=0 border=0 width=100%>
					<tr>				
						<td align=center>
							
							<?php if ($this->_tpl_vars['Article']['ArticleProducts'][$this->_sections['j']['index']]['Price_s'] > 0): ?><div style="color: brown; white-space: nowrap;" class="ArticlePost-product-price"><?php echo $this->_tpl_vars['Article']['ArticleProducts'][$this->_sections['j']['index']]['Price_s']; ?>
</div><?php endif; ?>	
								
							<?php if ($this->_tpl_vars['Article']['ArticleProducts'][$this->_sections['j']['index']]['product_code']): ?><div class="ArticlePost-product-code"><?php echo 'Арт.'; 
 echo $this->_tpl_vars['Article']['ArticleProducts'][$this->_sections['j']['index']]['product_code']; ?>
</div><?php endif; ?>
						</td>
					</tr>
				</table>	
			</td>
		<?php endfor; endif; ?>
	</table>
	</div>
<?php endif; ?>


	
<?php if ($this->_tpl_vars['Article']['ArticleArticles']): ?>
	<h2 class="ArticlePost-caption"><?php echo 'Рекомендуемые прочитать'; ?>
</h2>
	<div class="post_block ArticlePost-article" style="padding:10px">
	<?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['Article']['ArticleArticles']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>
		<a href="<?php echo ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL)."&CategoryID=".($this->_tpl_vars['Article']['ArticleArticles'][$this->_sections['j']['index']]['CategoryID'])."&CategorySlug=".($this->_tpl_vars['Article']['ArticleArticles'][$this->_sections['j']['index']]['CategorySlug'])."&postID=".($this->_tpl_vars['Article']['ArticleArticles'][$this->_sections['j']['index']]['ArticleID'])."&postSlug=".($this->_tpl_vars['Article']['ArticleArticles'][$this->_sections['j']['index']]['ArticleSlug']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
"><?php echo $this->_tpl_vars['Article']['ArticleArticles'][$this->_sections['j']['index']]['ArticleTitle']; ?>
</a><br>
	<?php endfor; endif; ?>
	</div>
<?php endif; ?>				
	
	
			
<?php if ($this->_tpl_vars['CommentsTypes']['vkontakte'] == 'true'): ?>	
	<h2 class="ArticlePost-caption"><?php echo 'Комментарии ВКонтакте'; ?>
</h2>	
	<div class="post_block ArticlePost-vkontakte" style="padding:10px">
	<script type="text/javascript" src="http://userapi.com/js/api/openapi.js?49"></script>

	<script type="text/javascript">
	  VK.init(<?php echo '{'; ?>
apiId: <?php echo @CONF_ARTCLE_COMMENTS_VK_ID; ?>
, onlyWidgets: true<?php echo '}'; ?>
);
	</script>

	<div id="vk_comments"></div>
	<script type="text/javascript">
	VK.Widgets.Comments("vk_comments", <?php echo '{'; ?>
limit: <?php echo @CONF_ARTCLE_COMMENTS_VK_COUNT; ?>
, width: "<?php echo @CONF_ARTCLE_COMMENTS_VK_WIDTH; ?>
", attach: "<?php if (@CONF_ARTCLE_COMMENTS_VK_ATTACH == ''): ?>false<?php else: 
 echo @CONF_ARTCLE_COMMENTS_VK_ATTACH; 
 endif; ?>"<?php echo '}'; ?>
);
	</script>
	</div>
<?php endif; ?> 
 
	
<?php if ($this->_tpl_vars['CommentsTypes']['facebook'] == 'true'): ?>	
	<h2 class="ArticlePost-caption"><?php echo 'Комментарии Facebook'; ?>
</h2>	
	<div class="post_block ArticlePost-facebook" style="padding:10px">
	<script src="//connect.facebook.net/en_US/all.js"></script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) <?php echo '{'; ?>

	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=<?php echo @CONF_ARTCLE_COMMENTS_FB_ID; ?>
";
	  fjs.parentNode.insertBefore(js, fjs);
	<?php echo '}'; ?>
(document, 'script', 'facebook-jssdk'));</script>
	<div class="fb-comments" data-href=" http://<?php echo @CONF_SHOP_URL; 
 echo ((is_array($_tmp="?ukey=".(@CONF_ARTCLE_URL)."&CategoryID=".($this->_tpl_vars['Article']['CategoryID'])."&CategorySlug=".($this->_tpl_vars['Article']['CategorySlug'])."&postID=".($this->_tpl_vars['Article']['ArticleID'])."&postSlug=".($this->_tpl_vars['Article']['ArticleSlug']))) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
" data-num-posts="<?php echo @CONF_ARTCLE_COMMENTS_FB_COUNT; ?>
" data-width="<?php echo @CONF_ARTCLE_COMMENTS_FB_WIDTH; ?>
" <?php if (@CONF_ARTCLE_COMMENTS_FB_THEME != 'light'): ?>data-colorscheme="<?php echo @CONF_ARTCLE_COMMENTS_FB_THEME; ?>
"<?php endif; ?>></div>
	</div>
<?php endif; ?> 
 
 
 <?php if ($this->_tpl_vars['CommentsTypes']['system'] == 'true'): ?>	
	<h2 class="ArticlePost-caption"><?php echo 'Комментарии'; ?>
</h2>	
	<div class="post_block ArticlePost-comments" style="padding:10px">
	<?php unset($this->_sections['c']);
$this->_sections['c']['name'] = 'c';
$this->_sections['c']['loop'] = is_array($_loop=$this->_tpl_vars['Comments']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['c']['show'] = true;
$this->_sections['c']['max'] = $this->_sections['c']['loop'];
$this->_sections['c']['step'] = 1;
$this->_sections['c']['start'] = $this->_sections['c']['step'] > 0 ? 0 : $this->_sections['c']['loop']-1;
if ($this->_sections['c']['show']) {
    $this->_sections['c']['total'] = $this->_sections['c']['loop'];
    if ($this->_sections['c']['total'] == 0)
        $this->_sections['c']['show'] = false;
} else
    $this->_sections['c']['total'] = 0;
if ($this->_sections['c']['show']):

            for ($this->_sections['c']['index'] = $this->_sections['c']['start'], $this->_sections['c']['iteration'] = 1;
                 $this->_sections['c']['iteration'] <= $this->_sections['c']['total'];
                 $this->_sections['c']['index'] += $this->_sections['c']['step'], $this->_sections['c']['iteration']++):
$this->_sections['c']['rownum'] = $this->_sections['c']['iteration'];
$this->_sections['c']['index_prev'] = $this->_sections['c']['index'] - $this->_sections['c']['step'];
$this->_sections['c']['index_next'] = $this->_sections['c']['index'] + $this->_sections['c']['step'];
$this->_sections['c']['first']      = ($this->_sections['c']['iteration'] == 1);
$this->_sections['c']['last']       = ($this->_sections['c']['iteration'] == $this->_sections['c']['total']);
?>	
		<div class="review_block ArticlePost-comment" id="post<?php echo $this->_tpl_vars['Comments'][$this->_sections['c']['index']]['CommentID']; ?>
">
 			
			<div class="review_date ArticlePost-comment-authorNdate"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['Comments'][$this->_sections['c']['index']]['CommentAuthor'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('linewrap', true, $_tmp) : smarty_modifier_linewrap($_tmp)); ?>
 (<?php echo $this->_tpl_vars['Comments'][$this->_sections['c']['index']]['CommentDate']; ?>
)</div>	
 			
			<div class="review_content ArticlePost-comment-text"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['Comments'][$this->_sections['c']['index']]['CommentText'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)))) ? $this->_run_mod_handler('linewrap', true, $_tmp) : smarty_modifier_linewrap($_tmp)); ?>
</div>
		</div>
		<br>
	<?php endfor; else: ?>
		<p><?php echo 'Нет отзывов к этой записи'; ?>
</p>
	<?php endif; ?>
	</div>
	
	<h2 class="ArticlePost-caption"><?php echo 'Написать отзыв'; ?>
</h2>

	<div class="post_block" style="padding:10px">
	<?php echo $this->_tpl_vars['MessageBlock']; ?>
	
		
	<form enctype="multipart/form-data" action="<?php echo ((is_array($_tmp='')) ? $this->_run_mod_handler('set_query_html', true, $_tmp) : smarty_modifier_set_query_html($_tmp)); ?>
" method="post" name="MainForm" id="Comment" >
		<input name="action" value="add_comment" type="hidden" />
		<input name="ArticleID" value="<?php echo $this->_tpl_vars['Article']['ArticleID']; ?>
" type="hidden" />
		<table>
			<tr>
 			
				<td colspan="2">
					<input type="text" name="CommentAuthor" value="<?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['Comment']['CommentAuthor'])) ? $this->_run_mod_handler('default', true, $_tmp, 'str_your_name') : smarty_modifier_default($_tmp, 'str_your_name')))) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>
" title="<?php echo 'Имя'; ?>
" onfocus="if(this.value=='<?php echo 'Имя'; ?>
') this.value=''" onblur="if(this.value=='') this.value='<?php echo 'Имя'; ?>
'">
				</td>
			</tr>	
			<tr>
 					
				<td colspan="2">
					<textarea name="CommentText" rows="8" cols="60"  title="<?php echo 'Ваш отзыв'; ?>
" onfocus="if(this.value=='<?php echo 'Ваш отзыв'; ?>
') this.value=''" onblur="if(this.value=='') this.value='<?php echo 'Ваш отзыв'; ?>
'"><?php echo ((is_array($_tmp=((is_array($_tmp=@$this->_tpl_vars['Comment']['CommentText'])) ? $this->_run_mod_handler('default', true, $_tmp, 'prddiscussion_body') : smarty_modifier_default($_tmp, 'prddiscussion_body')))) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>
</textarea>
				</td>
			</tr>
				
			<?php if (@CONF_ENABLE_CONFIRMATION_CODE): ?>
			<tbody id="reg_confcode">
			<tr>
				<td colspan="2"><?php echo 'Введите число, изображенное на рисунке'; ?>
</td>
			</tr>
			<tr>
				<td align="right">
					<img src="<?php echo @URL_ROOT; ?>
/imgval.php" alt="code" border="0">
				</td>
				<td align="left">
					<input name="fConfirmationCode" value="" type="text" >
				</td>
			</tr>
			</tbody>
			<?php endif; ?>
		</table>
		<input type="submit" value="<?php echo 'Отправить'; ?>
">	
	</form>	
	</div>
<?php endif; ?> 
